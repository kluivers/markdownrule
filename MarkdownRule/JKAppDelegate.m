//
//  JKAppDelegate.m
//  MarkdownRule
//
//  Created by Joris Kluivers on 5/22/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import "JKAppDelegate.h"

@implementation JKAppDelegate

@synthesize window = _window, webview;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"Example" withExtension:@"html"];
	NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
	
	[[self.webview mainFrame] loadRequest:request];
}

@end
